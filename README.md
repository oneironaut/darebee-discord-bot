# darebee-discord-bot

Discord bot that watches https://darebee.com for changes and announces new workouts and daily dares.  The bot can embed workouts in the chat and challenge users to daredice challenges.