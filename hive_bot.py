#!/usr/bin/env python3.8

import datetime
import random  # daredice
import discord
from discord.ext import tasks
from discord.commands.context import ApplicationContext
import certifi
import urllib3  # checking urls
from lxml import etree as ET  # parsing html
from utils import (DAREBEE_BASE_URL, SECTIONS,
                   replace_ending, load_dict, get_dict_from_html,
                   witty_roller, update_content)
from hive_secrets import BOT_TOKEN, ANNOUNCE_CHANNEL_ID


DAREDICE_REPS = ['4', '8', '10', '12', '16', '20']
DAREDICE_EXERCISES = ['push-ups', 'lunges', 'squats',
                      'jumping jacks', 'sit-ups', 'basic burpees']

# modified lightly to always include {name} so the rolling user gets tagged
# these are formatted for ?daredice with no target specified
DAREDICE_PHRASES = ["Lightning flashes in the distance. Rolling thunder shakes you to your core. Thor has challenged {name} to do {reps} {exercise}!",
                    "{name}'s got a new job as a night guard at the eerie museum down the road. Probably time to train up to flee from ghosts! Do {reps} {exercise}!",
                    "Most people call it exercise. This is called training for the zombie apocalypse. Time for {name} to perform {reps} {exercise}!",
                    "They never said getting accepted into a super hero academy would be easy, kid! Let's get started with {reps} {exercise}, {name}.",
                    "You can feel the caffeine flooding your blood stream! Time to work off all that extra energy and do {reps} {exercise}, {name}!",
                    "Maybe you can't outrun a bad diet, but you can always be prepared for zombies. Try out {reps} {exercise}, {name}.",
                    "Your friendly neighborhood healer wants to test that their heals worked! Time to perform {reps} {exercise} for them, {name}.",
                    "The wizard down the street sent a new bolt of energy to {name} for their cat's birthday! Keep up with yourself with {reps} {exercise}.",
                    "The earth quakes and bursts open beneath you. The landing is softer than you expected, but now Hades has challenged {name} to keep up with him! Show him who's boss with {reps} {exercise}!"]

BOT_DESCRIPTION = '''hive-bot is designed to help access content on DAREBEE.com.
Code by oneironaut
Daredice phrases by axolotl-dressed-like-a-punk

hive-bot can:
   - embed DAREBEE workouts with the /workout command.
   - track and announce the current daily dare with /dare command.
   - track and announce the workout of the day with /wod command.
   - give you a quick daredice challenge with the /daredice command.
'''

pool: urllib3.poolmanager.PoolManager = urllib3.PoolManager(
    num_pools=3,
    cert_reqs='CERT_REQUIRED',
    ca_certs=certifi.where()
)

bot = discord.Bot(description=BOT_DESCRIPTION)


@bot.event
async def on_ready():
    """Announce that the bot has started"""
    print(f"{bot.user} is ready and online!")


@bot.event
async def on_application_command_error(ctx: discord.ApplicationContext, error: discord.DiscordException):
    """Catch command errors"""
    print(error)
    print(ctx)
    raise error  # Here we raise other errors to ensure they aren't ignored

# region commands


@bot.command()
@discord.commands.option("workout_name", description="Name of workout from url ('death by burpees' or 'death-by-burpees')")
async def workout(ctx: ApplicationContext, workout_name: str):
    """Embed a darebee workout"""
    workoutname = replace_ending(workout_name, '.html', '')  # remove .html from end
    workoutname = replace_ending(workoutname, 'workout', '')  # remove workout from end
    workoutname = workoutname.replace("'", '')  # remove apostrophes
    # replace spaces with hyphens, strip spaces and hypens, lowercase
    workoutname = workoutname.replace(' ', '-').strip(' -').lower()
    workouturl = f"{DAREBEE_BASE_URL}/workouts/{workoutname}-workout.html"
    workoutimgurl = f"{DAREBEE_BASE_URL}/images/workouts/{workoutname}-workout-intro.jpg"
    em1 = discord.Embed(title=f"DAREBEE {workoutname} workout", url=workouturl)
    em1.set_image(url=workoutimgurl)
    await ctx.respond(embed=em1)


@bot.command()
async def dare(ctx: ApplicationContext):
    """Show current daily dare"""
    em1 = discord.Embed(title="Current daily dare", url=DAREBEE_BASE_URL+content_dicts['exercise']['link'])
    em1.set_image(url=DAREBEE_BASE_URL+content_dicts['exercise']['img'])
    await ctx.respond(embed=em1)


@bot.command()
async def wod(ctx: ApplicationContext):
    """Show current workout of the day"""
    em1 = discord.Embed(title="Current workout of the day", url=DAREBEE_BASE_URL+content_dicts['wod']['link'])
    em1.set_image(url=DAREBEE_BASE_URL+content_dicts['wod']['img'])
    await ctx.respond(embed=em1)


@bot.command()
@discord.commands.option("name", description="Name of discord user to throw dice at.  You can use @mention syntax.", required=False, default='')
async def daredice(ctx: ApplicationContext, name: str):
    """Roll the daredice ( https://www.darebee.com/daredice.html )."""

    rep_index = random.randint(0, len(DAREDICE_REPS)-1)
    exercise_index = random.randint(0, len(DAREDICE_EXERCISES)-1)

    if name == '':
        solo_phrase_index = random.randint(0, len(DAREDICE_PHRASES)-1)
        result = DAREDICE_PHRASES[solo_phrase_index].format(
            name=ctx.author.mention, reps=DAREDICE_REPS[rep_index], exercise=DAREDICE_EXERCISES[exercise_index])
    else:
        result = (f"{ctx.author.mention} tosses the magical daredice toward {name}: {DAREDICE_REPS[rep_index]} "
                  f"{DAREDICE_EXERCISES[exercise_index]}!")
    await ctx.respond(result)


@bot.command()
@discord.commands.option("number_of_sides", description="Number of sides on the die you will roll (default is 20)", required=False, default=20)
async def roll(ctx: ApplicationContext, number_of_sides: int):
    """Roll a die with optional number_of_sides (default is 20)"""
    await ctx.respond(witty_roller(ctx.author.mention, number_of_sides))


@bot.command()
async def time(ctx: ApplicationContext):
    """Show UTC time."""
    await ctx.respond(f'UTC time: {datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")}')

# endregion


@tasks.loop(seconds=600)
async def web_update():
    """check darebee.com for updates"""
    if not bot.is_closed():
        print(f"background task running. current_loop: {web_update.current_loop}")

        channel = bot.get_channel(ANNOUNCE_CHANNEL_ID)

        try:
            # grab the url data
            response: urllib3.response.HTTPResponse = pool.request(method='GET', url=DAREBEE_BASE_URL, timeout=1)
        except Exception as ex:
            print(f"Encountered an error fetching page: {ex}")
        else:
            # turn the data into something we can query with xpath syntax
            root: ET._Element = ET.HTML(response.data)

            # extract new data from html
            new_content_dicts = {}
            for section in SECTIONS:
                new_content_dicts[section[0]] = get_dict_from_html(root, section[0])

            for section in SECTIONS:
                await update_content(channel, content_dicts[section[0]], new_content_dicts[section[0]], 
                                    f"{section[0]}_dict.p", section[1])

            print("updated content")


@web_update.before_loop
async def web_update_wait():
    """Make sure bot is ready before task loop starts"""
    await bot.wait_until_ready()
    print("bot is ready")

content_dicts = {}

# load dictionaries from pickle files
for section in SECTIONS:
    content_dicts[section[0]] = load_dict(f"{section[0]}_dict.p")

web_update.start()
bot.run(BOT_TOKEN)
