import logging
import urllib3
from lxml import etree as ET  # parsing html
import certifi
import pytest
from utils import DAREBEE_BASE_URL, SECTIONS, BLANK_DICT, load_dict, get_dict_from_html, witty_roller


pool: urllib3.poolmanager.PoolManager = urllib3.PoolManager(
    num_pools=3,
    cert_reqs='CERT_REQUIRED',
    ca_certs=certifi.where()
)


def test_get_dict_from_html():
    """test get_dict"""
    logging.info(DAREBEE_BASE_URL)

    logging.info("about to request")

    response: urllib3.response.HTTPResponse = pool.request(method='GET', url=DAREBEE_BASE_URL, timeout=1)

    # logging.info(f"{response.data}")

    root: ET._Element = ET.HTML(response.data)

    content_dicts = {}

    # extract data from html
    for section in SECTIONS:
        content_dicts[section[0]] = get_dict_from_html(root, section[0])
        logging.info(content_dicts[section[0]])
        assert content_dicts[section[0]]['link'] != ''


def test_load_dict_default():
    dictionary = load_dict('invalid_file.name')
    assert dictionary == BLANK_DICT


TEST_CASES = [
    (-1, "mobius"),
    (0, "mobius"),
    (1, "mobius"),
    (2, "coin"),
    (3, "d3"),
    (7, "d7"),
    (1000000, "sphere"),
    (999999999, "sphere")
]
@pytest.mark.parametrize("sides,expected_result", TEST_CASES)
def test_witty_roller(sides: int, expected_result: str):
    
    result = witty_roller("user", sides)
    logging.info(f"{result}")
    assert expected_result in result
