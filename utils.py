import logging
import pickle
import discord
import random
from lxml import etree as ET  # parsing html
from pathlib import Path

DAREBEE_BASE_URL = 'https://darebee.com'

# ccs id name, whether or not to pin post
SECTIONS = [
    ('wod', False),
    ('challenge', True),
    ('exercise', False),
    ('promo', True),
    ('community', True)
]

BLANK_DICT = {
    'title': '',
    'link': '',
    'img': '',
    'msg_id': 0
}


def replace_ending(sentence: str, old: str, new: str):
    """https://stackoverflow.com/a/61550058"""
    if sentence.endswith(old):
        return sentence[:-len(old)] + new
    return sentence


def load_dict(dict_path: str, default_dict: dict = BLANK_DICT) -> dict:
    """load dictionary from pickle file"""
    dict_path: Path = Path(dict_path)
    if dict_path.is_file():
        dictionary = pickle.load(open(dict_path, "rb"))
    else:
        dictionary = default_dict
    print(dictionary)
    return dictionary


def get_dict_from_html(root: ET._Element, css_id: str) -> dict:
    """find dict from html etree based on id"""
    title_results = root.xpath('//div[@id="' + css_id + '"]/div/p/a/img/@alt')
    title = title_results[0] if len(title_results) > 0 else ''

    dictionary = {}

    # syntax for selecting div by class - https://stackoverflow.com/a/34779105
    dictionary = {
        'title': title,
        'link': root.xpath('//div[@id="' + css_id + '"]/div/p/a/@href')[0],
        'img': root.xpath('//div[@id="' + css_id + '"]/div/p/a/img/@src')[0],
    }

    if dictionary['title'] == '':
        dictionary['title'] = css_id.capitalize()

    logging.debug(f"{dictionary}")

    return dictionary


def witty_roller(roller_name: str, number_of_sides: int) -> str:
    """A bit of wit for rolling a die."""
    result: str = ''
    msg: str = ''
    if (number_of_sides >= 1000000):
        number_of_sides = 1000000
        result = random.randint(1, number_of_sides)
        msg = f"{roller_name} rolls a nearly perfect million sided sphere: {result}"
    elif (number_of_sides == 2):
        if (random.randint(0,1) == 0):
            result = "heads"
        else:
            result = "tails"

        msg = f"{roller_name} tosses a coin: {result}"
    elif (number_of_sides <= 1):
        number_of_sides = 1
        msg = f"{roller_name} tosses a mobius strip and it lands on its side."
    else:
        result = random.randint(1, number_of_sides)

        msg = f"{roller_name} rolls a d{number_of_sides}: {result}"
    return msg


async def unpin_message(channel, message_id):
    """unpin a message"""
    print("trying to unpin message: " + str(message_id))
    try:
        old_pin_msg = await channel.fetch_message(message_id)
        print("unpinning message: " + str(message_id))
        await old_pin_msg.unpin()
    except:
        print("couldn't find old msg by id: " + str(message_id))


async def post_and_pin_message(channel, old_dict, em, pin):
    """post and pin a message"""
    try:
        new_message = await channel.send(embed=em)  # announce to channel
        old_dict['msg_id'] = new_message.id
        if pin:
            await new_message.pin()
    except:
        print("couldn't post or pin message")


async def update_content(channel, old_dict, new_dict, dict_path, pin):
    """update content in the updates channel"""
    old_pin_id = 0
    is_new_content = False
    try:
        # check if dict link has changed
        if (old_dict['link'] != new_dict['link']):
            old_dict['title'] = new_dict['title']
            old_dict['link'] = new_dict['link']
            old_dict['img'] = new_dict['img']

            old_pin_id = old_dict['msg_id']
            is_new_content = True
    except KeyError:
        print("Key Error in update_content, ignoring.")
    else:
        # no KeyError exception
        # has there actually been an update?
        if (is_new_content):
            # make post with embed and set image
            em1 = discord.Embed(
                title=old_dict['title']+" (click here)", url=DAREBEE_BASE_URL+old_dict['link'])
            em1.set_image(url=DAREBEE_BASE_URL+old_dict['img'])

            # unpin old message
            if pin and old_pin_id != 0:
                await unpin_message(channel, old_pin_id)

            # post and pin message
            await post_and_pin_message(channel, old_dict, em1, pin)

            # save to file so we don't reannounce
            pickle.dump(old_dict, open(dict_path, "wb"))
    finally:
        # update content done
        pass
